# Syllabus #

### Description ###

This topic introduces techniques developers can leverage to debug, instrument, monitor, and test applications. 

Let's collaborate and learn from each other. 

### Topic Objectives ###
Introduce students to 

1. the debugging process using Visual Studio
2. regression tests 
3. instrumentation using logging libraries 
4. crosscutting concerns 
5. log management to monitor application health, and identify and diagnose problems in production environments 

### Topics ###

1. Lab setup and introductions ~15 min
	* Verify Git CLI
	* Pull and build code
		* https://bitbucket.org/codeethic/dedux
		* `dotnet run`	
	* Verify Seq installation
		* http://localhost:5341 		
2. Exercise 1: Finding a logic error ~30 
	* Challenge: find the logic error ~10
	* Introduce debugging basics in Visual Studio ~20
3. Lab 1: Unit Testing ~30 min
	* Adding unit tests to expose defects and prevent regressions 
4. Lab 2: Adding instrumentation ~60 min
	* Logging basics with Serilog 
		* Sinks 
	* Log levels
	* Crosscutting concerns (aspects), decorator pattern, and interception 
	* Introduction to log managment using Seq 
5. Exercise 2: Null Reference Exception ~30 min
	* Identify an NRE defect in deployed code 
6. Lab 3: Adding exception handling ~30 min
	* Using techniques from Lab 2, implement an exception interceptor 
7. Example of real-world defect identified using topic techniques 
	* How I used these techniques to identify a nasty defect in our LMS code 
8. Wrap-Up ~15 min

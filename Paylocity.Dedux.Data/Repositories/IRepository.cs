﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Paylocity.Dedux.Core.Model;
using System.Linq.Expressions;
using System;

namespace Paylocity.Dedux.Data.Repositories
{
    public interface IRepository<TEntity>
       where TEntity : class, IEntity
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(int id);
        Task<List<TEntity>> FindAllByAsync(Expression<Func<TEntity, bool>> predicate);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Delete(TEntity entity);
        void DeleteRange(IEnumerable<TEntity> range);
    }
}
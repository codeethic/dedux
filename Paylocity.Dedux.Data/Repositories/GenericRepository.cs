﻿using Microsoft.EntityFrameworkCore;
using Paylocity.Dedux.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Data.Repositories
{
    internal class GenericRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        internal GenericRepository(AppDbContext context)
        {
            Context = context;
        }

        protected AppDbContext Context { get; }

        protected DbSet<TEntity> DbSet => Context.Set<TEntity>();

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<TEntity>> FindAllByAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await DbSet.Where(predicate).ToListAsync();
        }

        public void Update(TEntity entity)
        {
            var entry = Context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            SetAsModified(entity);
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }

        public virtual void Delete(TEntity entity)
        {
            SetAsDeleted(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> range)
        {
            if (range == null) return;
            foreach (var entity in range)
            {
                Delete(entity);
            }
        }

        protected void SetAsModified(IEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        protected void SetAsDeleted(IEntity entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
        }
    }
}


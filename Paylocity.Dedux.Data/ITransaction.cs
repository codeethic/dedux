﻿using System;

namespace Paylocity.Dedux.Data
{
    public interface ITransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}

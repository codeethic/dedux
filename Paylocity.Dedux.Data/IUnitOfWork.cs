﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Data.Repositories;
using System;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;
        ITransaction BeginTransaction();
        Task SaveChangesAsync();
    }
}

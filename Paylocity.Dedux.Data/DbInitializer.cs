﻿using Paylocity.Dedux.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Paylocity.Dedux.Data
{
    public class DbInitializer
    {
        private AppDbContext _context;

        public DbInitializer(AppDbContext context)
        {
            _context = context;
        }
        
        public void Seed()
        {
            if (!_context.Employees.Any())
            {
                _context.AddRange(_employees);
                _context.SaveChanges();
            }
        }
        
        List<Employee> _employees = new List<Employee>
        {
            new Employee
            {
                FirstName = "Anakin",
                LastName = "Skywalker",
                Dependents = new List<Dependent>
                {
                    new Dependent
                    {
                        FirstName = "Luke",
                        LastName = "Skywalker"
                    },
                    new Dependent
                    {
                        FirstName = "Leia",
                        LastName = "Skywalker"
                    }
                }
            }, 
            new Employee
            {
                FirstName = "Josh",
                LastName = "Friend",
                Dependents = new List<Dependent>
                {
                    new Dependent
                    {
                        FirstName = "Jacob",
                        LastName = "Friend"
                    },
                    new Dependent
                    {
                        FirstName = "Alexander",
                        LastName = "Friend"
                    }
                }
            },
            new Employee
            {
                FirstName = "Tamara",
                LastName = "Stephenson",
                Dependents = new List<Dependent>
                {
                    new Dependent
                    {
                        FirstName = "Mikey",
                        LastName = "Stephenson"
                    }
                }
            },
            new Employee
            {
                FirstName = "Chad",
                LastName = "Lazette",
                Dependents = new List<Dependent>
                {
                    new Dependent
                    {
                        FirstName = "Chad Michael",
                        LastName = "Lazette"
                    }
                }
            },
            new Employee
            {
                FirstName = "Bob",
                LastName = "Horn"
            },
            new Employee
            {
                FirstName = "Sam",
                LastName = "Rizvi"
            }
        };
    }
}

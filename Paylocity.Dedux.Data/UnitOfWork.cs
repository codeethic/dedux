﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Data.Repositories;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(AppDbContext context)
        {
            Context = context;
        }
        
        internal AppDbContext Context { get; }

        public ITransaction BeginTransaction()
        {
            return new Transaction(Context.Database.BeginTransaction());
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            // TODO: cache
            return new GenericRepository<TEntity>(Context);
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
﻿using Moq;
using Paylocity.Dedux.Core.Model;
using Xunit;

namespace Paylocity.Dedux.Data.Tests
{    
    public class DependentTests : BaseDataTest<Dependent>
    {
        [Fact]
        public void DependentCreate()
        {
            var dependent = CreateDependent();
            AddTest(dependent);

            _repository.Verify(r => r.Add(It.Is<Dependent>(e =>
                e.Id == dependent.Id
            )));
        }

        [Fact]
        public void DependentUpdate()
        {
            var dependent = CreateDependent();
            UpdateTest(dependent);

            _repository.Verify(r => r.Update(It.Is<Dependent>(e =>
                e.FirstName == dependent.FirstName
            )));
        }

        private static Dependent CreateDependent()
        {
            return new Dependent
            {
                Id = 1,
                FirstName = "Foo Jr.",
                LastName = "Bar"
            };
        }
    }
}

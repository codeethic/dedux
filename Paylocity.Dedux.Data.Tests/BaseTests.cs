﻿using Moq;
using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Data.Repositories;

namespace Paylocity.Dedux.Data.Tests
{
    public class BaseDataTest<TEntity> where TEntity : class, IEntity
    {
        Mock<ITransaction> _transaction;
        Mock<IUnitOfWork> _unitOfWork;
        protected Mock<IRepository<TEntity>> _repository;
        
        public BaseDataTest()
        {
            _transaction = new Mock<ITransaction>();
            _unitOfWork = new Mock<IUnitOfWork>();
            _repository = new Mock<IRepository<TEntity>>();

            _unitOfWork.Setup(uow => uow.GetRepository<TEntity>()).Returns(_repository.Object);
            _unitOfWork.Setup(uow => uow.BeginTransaction()).Returns(_transaction.Object);
        }

        public void AddTest(TEntity entity)
        {
            var uow = _unitOfWork.Object;
            
            using (var transaction = uow.BeginTransaction())
            {
                uow.GetRepository<TEntity>().Add(entity);
                uow.SaveChangesAsync();
                transaction.Commit();
            }

            _repository.Setup(r => r.Add(It.IsAny<TEntity>()));
        }

        public void UpdateTest(TEntity entity)
        {
            var uow = _unitOfWork.Object;

            using (var transaction = uow.BeginTransaction())
            {
                uow.GetRepository<TEntity>().Update(entity);
                uow.SaveChangesAsync();
                transaction.Commit();
            }

            _repository.Setup(r => r.Update(It.IsAny<TEntity>()));
        }
    }
}

﻿using Moq;
using Paylocity.Dedux.Core.Model;
using Xunit;

namespace Paylocity.Dedux.Data.Tests
{    
    public class EmployeeTests : BaseDataTest<Employee>
    {
        [Fact]
        public void EmployeeCreate()
        {
            var employee = new Employee
            {
                Id = 1,
                FirstName = "Foo",
                LastName = "Bar"
            };

            AddTest(employee);

            _repository.Verify(r => r.Add(It.Is<Employee>(e =>
                e.Id == 1
            )));
        }

        [Fact]
        public void EmployeeUpdate()
        {
            var dependent = new Employee
            {
                Id = 1,
                FirstName = "Foo Jr.",
                LastName = "Bar"
            };
            
            UpdateTest(dependent);

            _repository.Verify(r => r.Update(It.Is<Employee>(e =>
                e.FirstName == "Foo Jr."
            )));
        }
    }
}

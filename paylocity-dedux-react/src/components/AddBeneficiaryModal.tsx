import * as React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Beneficiary } from '../types/interfaces';
import { BeneficiaryForm } from './BeneficiaryForm';

export interface Props {
    title: string;
    employeeId?: number;
    onSave: (b: Beneficiary) => void;
}

interface State {
    firstName: string;
    lastName: string;
    showModal: boolean;
}

export class AddBeneficiaryModal extends React.Component<Props, State> {

    public state: State = {
        firstName: '',
        lastName: '',
        showModal: false
    };

    public constructor(props: Props) {
        super(props);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
    }

    handleFirstNameChange(name: string) {
        this.setState({ firstName: name });
    }

    handleLastNameChange(name: string) {
        this.setState({ lastName: name });
    }

    saveChanges() {
        var beneficiary = {
            firstName: this.state.firstName,
            lastName: this.state.lastName
        };
        this.props.onSave(beneficiary);
        this.close();
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

    render() {
        return (
            <div>
                <p>
                    <Button bsStyle="primary" onClick={this.open}>
                        {this.props.title}
                    </Button>
                </p>
                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton="true">
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <BeneficiaryForm
                            firstName=""
                            lastName=""
                            onFirstNameChange={(firstName) => this.handleFirstNameChange(firstName)}
                            onLastNameChange={(lastName) => this.handleLastNameChange(lastName)}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="primary" onClick={this.saveChanges}>
                            Save changes
            </Button>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default AddBeneficiaryModal;

import * as React from "react";
import { Table, Glyphicon, Grid, Row, Col, PageHeader } from "react-bootstrap";
import { Beneficiary } from "../types/interfaces";
import { AddBeneficiaryModal } from "./AddBeneficiaryModal";

interface Props {
  employees: Array<any>;
  onSaveEmployee: (b: Beneficiary) => void;
  onEditEmployee: (employeeId: number) => void;
  onDeleteEmployee: (employeeId: number) => void;
}

function EmployeeList({
  employees,
  onSaveEmployee,
  onEditEmployee,
  onDeleteEmployee
}: Props) {
  return (
    <Grid>
      <Row>
        <Col xs={14} md={5}>
          <PageHeader>
            <small>Enrolled Employees</small>
          </PageHeader>
          <Grid>
            <Row>
              <Col xs={1} md={3}>
                <AddBeneficiaryModal
                  onSave={b => onSaveEmployee(b)}
                  title="Add Employee"
                />
              </Col>
              <Col xs={1} md={3}>
                <table>
                  <tbody>
                    <tr>
                      <td>Employees:</td>
                      <td>{employees.length}</td>
                    </tr>
                  </tbody>
                </table>
              </Col>
            </Row>
          </Grid>
        </Col>
      </Row>
      <Row className="show-grid">
        <Col xs={14} md={9}>
          <div>
            <div>
              <Table striped="true" condensed="true" hover="true">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Details</th>
                  </tr>
                </thead>
                <tbody>
                  {employees.map(employee => (
                    <tr key={employee.id}>
                      <td>{employee.id}</td>
                      <td>{employee.firstName}</td>
                      <td>{employee.lastName}</td>
                      <td>
                        <button onClick={() => onEditEmployee(employee.id)}>
                          <Glyphicon glyph="eye-open" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </Col>
      </Row>
    </Grid>
  );
}

export default EmployeeList;

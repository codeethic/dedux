import * as React from 'react';
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

export interface Props {
    firstName: string;
    lastName: string;
    onFirstNameChange: (name: string) => void;
    onLastNameChange: (name: string) => void;
}

interface State {
    firstName: string;
    lastName: string;
}

export class BeneficiaryForm extends React.Component<Props, State> {

    public state: State = {
        firstName: '',
        lastName: ''
    };

    public constructor(props: Props) {
        super(props);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.getValidationState = this.getValidationState.bind(this);
    }

    handleFirstNameChange(e: React.FormEvent<HTMLInputElement>) {
        this.props.onFirstNameChange(e.currentTarget.value);
        this.setState({ firstName: e.currentTarget.value });
    }

    handleLastNameChange(e: React.FormEvent<HTMLInputElement>) {
        this.props.onLastNameChange(e.currentTarget.value);
        this.setState({ lastName: e.currentTarget.value });
    }

    getValidationState() {
        const fLength = this.state.firstName.length;
        const lLength = this.state.lastName.length;
        if (fLength > 0 && lLength > 0) {
            return 'success';
        }
        return 'error';
    }

    render() {
        return (
            <form>
                <FormGroup
                    controlId="formAddBeneficiary"
                    validationState={this.getValidationState()}
                >
                    <ControlLabel>First Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={this.state.firstName}
                        placeholder="Enter first name"
                        onChange={this.handleFirstNameChange}
                    />
                    <ControlLabel>Last Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={this.state.lastName}
                        placeholder="Enter last name"
                        onChange={this.handleLastNameChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
            </form>
        );
    }
}

export default BeneficiaryForm;

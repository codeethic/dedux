import * as React from "react";
import { Table, Glyphicon, Grid, Row, Col, PageHeader } from "react-bootstrap";
import { Beneficiary, Employee } from "../types/interfaces";
import { AddBeneficiaryModal } from "./AddBeneficiaryModal";

export interface Props {
  employee: Employee;
  dependents: any;
  onSaveDependent: (beneficiary: Beneficiary) => void;
  onEditEmployee: (dependentId: number) => void;
  onDeleteEmployee: (dependentId: number) => void;
}

function getHeader(employee: Employee) {
  if (employee != null) {
    var e = employee;
    return (
      <p>
        Employee: {e.firstName} {e.lastName}
      </p>
    );
  }

  return "Choose an employee to view details.";
}

function EmployeeDetails({
  employee,
  dependents,
  onSaveDependent,
  onEditEmployee,
  onDeleteEmployee
}: Props) {
  const rows = dependents.dependents ? dependents.dependents : [];

  var total = 0;
  if (employee == null) {
    return <p />;
  }

  return (
    <Grid>
      <Row>
        <Col xs={14} md={9}>
          <PageHeader>
            <small>{getHeader(employee)}</small>
          </PageHeader>
          <Grid>
            <Row>
              <Col xs={1} md={3}>
                <AddBeneficiaryModal
                  onSave={b => {
                    b.employeeId = employee.id;
                    onSaveDependent(b);
                  }}
                  title="Add Beneficiary"
                />
              </Col>
              <table>
                <tbody>
                  <tr>
                    <td>Salary:</td>
                    <td>
                      {" "}
                      {employee.annualSalary.toLocaleString(
                        navigator.language,
                        { style: "currency", currency: "USD" }
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>Annual Benefit Costs:</td>
                    <td>
                      {" "}
                      {employee.benefitCosts.toLocaleString(
                        navigator.language,
                        { style: "currency", currency: "USD" }
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>Period Benefit Costs:</td>
                    <td>
                      {" "}
                      {employee.periodBenefitCosts.toLocaleString(
                        navigator.language,
                        {
                          style: "currency",
                          currency: "USD"
                        }
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
            </Row>
          </Grid>
        </Col>
      </Row>
      <Row className="show-grid">
        <Col xs={14} md={9}>
          <div>
            <div>
              <Table striped="true" condensed="true" hover="true">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Annual Cost</th>
                    <th>Per Period</th>
                  </tr>
                </thead>
                <tbody>
                  {rows.map(d => (
                    <tr key={d.id}>
                      <td>{d.id}</td>
                      <td>{d.firstName}</td>
                      <td>{d.lastName}</td>
                      <td>
                        {d.benefitCosts.toLocaleString(navigator.language, {
                          style: "currency",
                          currency: "USD"
                        })}
                      </td>
                      <td>
                        {d.periodBenefitCosts.toLocaleString(
                          navigator.language,
                          { style: "currency", currency: "USD" }
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </Col>
      </Row>
    </Grid>
  );
}

export default EmployeeDetails;

import * as React from "react";
import * as ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import * as request from "superagent";
import { getStore } from "./reducers/employees";
import "./index.css";
import * as constants from "./constants";
import { Panel } from "react-bootstrap";
import Employees from "./containers/Employees";
import EmployeeDetails from "./containers/EmployeeDetails";

let store = getStore();

const title = <h3>Paylocity Dedux</h3>;

const panel = (
  <Panel header={title} bsStyle="primary">
    <Employees />
    <EmployeeDetails />
  </Panel>
);

ReactDOM.render(
  <Provider store={store}>
    <div>{panel}</div>
  </Provider>,
  document.getElementById("root") as HTMLElement
);

store.dispatch({ type: constants.GET_EMPLOYEE_DATA });
registerServiceWorker();

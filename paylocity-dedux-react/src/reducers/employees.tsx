import * as request from "superagent";
import { combineReducers, createStore } from "redux";
import * as actions from "../actions/";
import * as constants from "../constants";
import { Employee, Beneficiary, EmployeeDetails } from "../types/interfaces";

const apiRoot: string = "/api/";

const employees = (state: Array<Employee> = [], action) => {
  const rootRoute = "Employees";
  switch (action.type) {
    case constants.ADD_EMPLOYEE_DATA: {
      doPost(
        rootRoute,
        action.beneficiary,
        constants.ADD_EMPLOYEE_DATA_RECEIVED
      );
      return state;
    }
    case constants.ADD_EMPLOYEE_DATA_RECEIVED:
      return [
        ...state,
        {
          id: action.data.id,
          firstName: action.data.firstName,
          lastName: action.data.lastName
        }
      ];
    case constants.GET_EMPLOYEE_DATA: {
      doGet(rootRoute, constants.GET_EMPLOYEE_DATA_RECIEVED);
      return state;
    }
    case constants.GET_EMPLOYEE_DATA_RECIEVED: {
      return action.data;
    }
    case constants.UPDATE_EMPLOYEE_DATA: {
      const route = rootRoute + "/" + action.employeeId;
      doUpdate(route, constants.UPDATE_EMPLOYEE_DATA_SUCCESS);
      return state;
    }
    case constants.UPDATE_EMPLOYEE_DATA_SUCCESS: {
      alert("update the object in state");
      return state;
    }
    case constants.DELETE_EMPLOYEE_DATA: {
      doDelete(
        rootRoute,
        action.employeeId,
        constants.DELETE_EMPLOYEE_DATA_SUCCESS
      );
      return state;
    }
    case constants.DELETE_EMPLOYEE_DATA_SUCCESS: {
      var newState = Object.assign([], state, [
        ...state.filter(item => item.id !== action.deletedId)
      ]);
      return newState;
    }
    default:
      return state;
  }
};

const initData: EmployeeDetails = {
  dependents: Array<Beneficiary>(),
  firstName: "",
  lastName: ""
};

const dependents = (state: EmployeeDetails = initData, action) => {
  const rootRoute = "Dependents";
  switch (action.type) {
    case constants.ADD_DEPENDENT_DATA: {
      doPost(
        rootRoute,
        action.beneficiary,
        constants.ADD_DEPENDENT_DATA_RECEIVED
      );
      return state;
    }
    case constants.ADD_DEPENDENT_DATA_RECEIVED:
      return {
        employee: state.employee,
        dependents: [
          ...state.dependents,
          {
            id: action.data.id,
            firstName: action.data.firstName,
            lastName: action.data.lastName,
            benefitCosts: action.data.benefitCosts,
            periodBenefitCosts: action.data.periodBenefitCosts
          }
        ]
      };
    case constants.GET_DEPENDENTS_DATA: {
      const route = rootRoute + "/policyHolder/" + action.employeeId;
      doGet(route, constants.GET_DEPENDENTS_DATA_RECEIVED);
      return state;
    }
    case constants.GET_DEPENDENTS_DATA_RECEIVED: {
      return action.data;
    }
    case constants.UPDATE_DEPENDENT_DATA: {
      const route = rootRoute + "/" + action.id;
      doUpdate(route, constants.UPDATE_DEPENDENT_DATA_SUCCESS);
      return state;
    }
    case constants.UPDATE_DEPENDENT_DATA_SUCCESS: {
      alert("TODO: update the object state");
      return state;
    }
    case constants.DELETE_DEPENDENT_DATA: {
      doDelete(rootRoute, action.id, constants.DELETE_DEPENDENT_DATA_SUCCESS);
      return state;
    }
    case constants.DELETE_DEPENDENT_DATA_SUCCESS: {
      var newState = {
        employee: state.employee,
        dependents: Object.assign([], state, [
          ...state.dependents.filter(item => item.id !== action.deletedId)
        ])
      };
      return newState;
    }
    default:
      return state;
  }
};

function doGet(route: string, getReceivedActionType: string) {
  request.get(apiRoot + route).end((err, res) => {
    if (err) {
      store.dispatch({ type: constants.ERROR, err });
    }
    const data = JSON.parse(res.text);
    store.dispatch({
      type: getReceivedActionType,
      data
    });
  });
}

function doPost(route: string, payload, responseActionType: string) {
  request
    .post(apiRoot + route)
    .send(payload)
    .end((err, res) => {
      if (err) {
        store.dispatch({ type: constants.ERROR, err });
      }
      const data = JSON.parse(res.text);
      store.dispatch({
        type: responseActionType,
        data
      });
    });
}

function doUpdate(route: string, responseActionType: string) {
  request.put(apiRoot + route).end((err, res) => {
    if (err) {
      store.dispatch({ type: constants.ERROR, err });
    }
    store.dispatch({
      type: responseActionType
    });
  });
}

function doDelete(
  route: string,
  deletedId: number,
  responseActionType: string
) {
  request.delete(apiRoot + route + "/" + deletedId).end((err, res) => {
    if (err) {
      store.dispatch({ type: constants.ERROR, err });
    }
    store.dispatch({
      type: responseActionType,
      deletedId: deletedId
    });
  });
}

const deduxReducer = combineReducers({
  employees,
  dependents
});

export const store = createStore(deduxReducer);

export function getStore() {
  return store;
}

export default employees;

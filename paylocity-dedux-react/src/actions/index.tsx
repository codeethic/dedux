import * as constants from '../constants';
import { Beneficiary } from '../types/interfaces';

export interface AddEmployeeData {
    type: constants.ADD_EMPLOYEE_DATA;
    beneficiary: Beneficiary;
}

export function addEmployee(beneficiary: Beneficiary): AddEmployeeData {
    return {
        type: constants.ADD_EMPLOYEE_DATA,
        beneficiary: beneficiary
    };
}

export interface AddEmployeeDataReceived {
    type: constants.ADD_EMPLOYEE_DATA_RECEIVED;
    beneficiary: Beneficiary;
}

export function addEmployeeReceived(beneficiary: Beneficiary): AddEmployeeDataReceived {
    return {
        type: constants.ADD_EMPLOYEE_DATA_RECEIVED,
        beneficiary: beneficiary
    };
}

export interface UpdateEmployeeData {
    type: constants.UPDATE_EMPLOYEE_DATA;
    benficiary: Beneficiary;
}

export function updateEmployeeData(beneficiary: Beneficiary): UpdateEmployeeData {
    return {
        type: constants.UPDATE_EMPLOYEE_DATA,
        benficiary: beneficiary
    };
}

export interface DeleteEmployeeData {
    type: constants.DELETE_EMPLOYEE_DATA;
    employeeId: number;
}

export function deleteEmployeeData(employeeId: number): DeleteEmployeeData {
    return {
        type: constants.DELETE_EMPLOYEE_DATA,
        employeeId: employeeId
    };
}

export interface GetDependentsData {
    type: constants.GET_DEPENDENTS_DATA;
    employeeId: number;
}

export function getDependentsData(employeeId: number): GetDependentsData {
    return {
        type: constants.GET_DEPENDENTS_DATA,
        employeeId: employeeId
    };
}

export interface AddDependentData {
    type: constants.ADD_DEPENDENT_DATA;
    beneficiary: Beneficiary;
}

export function addDependent(beneficiary: Beneficiary): AddDependentData {
    return {
        type: constants.ADD_DEPENDENT_DATA,
        beneficiary: beneficiary
    };
}

export interface DeleteDependentData {
    type: constants.DELETE_DEPENDENT_DATA;
    dependentId: number;
}

export function deleteDependentData(dependentId: number): DeleteDependentData {
    return {
        type: constants.DELETE_DEPENDENT_DATA,
        dependentId: dependentId
    };
}

export type DeduxAction = AddEmployeeData | AddDependentData |
    GetDependentsData |
    DeleteEmployeeData | DeleteDependentData;


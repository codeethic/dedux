import EmployeeList from '../components/EmployeeList';
import Beneficiary from '../types/interfaces';
import * as actions from '../actions/';
import { connect, Dispatch } from 'react-redux';

export function mapStateToProps(state) {
    return {
        employees: state.employees
    };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.DeduxAction>) {
    return {
        onSaveEmployee: (beneficiary: Beneficiary) => dispatch(actions.addEmployee(beneficiary)),
        onEditEmployee: (employeeId: number) => dispatch(actions.getDependentsData(employeeId)),
        onDeleteEmployee: (employeeId: number) => dispatch(actions.deleteEmployeeData(employeeId)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeList);
import EmployeeDetails from '../components/EmployeeDetails';
import Beneficiary from '../types/interfaces';
import * as actions from '../actions/';
import { connect, Dispatch } from 'react-redux';

export function mapStateToProps(state) {
    return {
        employee: state.dependents.employee,
        dependents: state.dependents
    };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.DeduxAction>) {
    return {
        onSaveDependent: (beneficiary: Beneficiary) => dispatch(actions.addDependent(beneficiary)),
        onEditEmployee: (dependentId: number) => dispatch(actions.getDependentsData(1)),
        onDeleteEmployee: (dependentId: number) => dispatch(actions.deleteDependentData(dependentId)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeDetails);
export interface Employee {
  id?: number;
  firstName: string;
  lastName: string;
  periodBenefitCosts?: number;
  benefitCosts?: number;
  annualSalary?: number;
}

export interface Beneficiary extends Employee {
  employeeId?: number;
}

export interface EmployeeDetails extends Beneficiary {
  employee?: Employee;
  dependents: Array<Beneficiary>;
}

export default Employee;

using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Services;
using Paylocity.Dedux.Core.Rules;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using Moq;

namespace Paylocity.Dedux.Core.Tests
{
    public class CalculationsTests
    {
        [Fact]
        public void NoDiscountGivenNonAName()
        {
            var employee = new Employee() { FirstName = "Foo", LastName = "Bar" };
            var dependent = new Dependent() { FirstName = "Foo Jr.", LastName = "Bar" };

            var beneficiaries = ComputeCosts(employee, dependent);
            beneficiaries[0].BenefitCosts.ShouldBeEquivalentTo(1000m, because: "The cost of benefits is $1000/year for each employee");
            beneficiaries[1].BenefitCosts.ShouldBeEquivalentTo(500m, because: "The cost of benefits is $500/year for each employee");
        }

        [Fact]
        public void DiscountedGivenAFirstName()
        {
            var employee = new Employee() { FirstName = "AFoo", LastName = "Bar" };
            var dependent = new Dependent() { FirstName = "AFoo Jr.", LastName = "Bar" };

            var beneficiaries = ComputeCosts(employee, dependent);
            beneficiaries[0].BenefitCosts.ShouldBeEquivalentTo(1000m * 0.9m, because: "A name discount is applied");
            beneficiaries[1].BenefitCosts.ShouldBeEquivalentTo(500m * 0.9m, because: "A name discount is applied");
        }

        [Fact]
        public void DiscountedGivenALastName()
        {
            var employee = new Employee() { FirstName = "Foo", LastName = "aBar" };
            var dependent = new Dependent() { FirstName = "Foo Jr.", LastName = "aBar" };
            List<Beneficiary> beneficiaries = ComputeCosts(employee, dependent);
            beneficiaries[0].BenefitCosts.ShouldBeEquivalentTo(1000m * 0.9m);
            beneficiaries[0].PeriodBenefitCosts.ShouldBeEquivalentTo((1000m * 0.9m) / 26);
            beneficiaries[1].BenefitCosts.ShouldBeEquivalentTo(500m * 0.9m);
            beneficiaries[1].PeriodBenefitCosts.ShouldBeEquivalentTo((500m * 0.9m) / 26);
        }

        [Fact]
        public void CheckUndiscountedEmployee()
        {
            var employee = new Employee() { FirstName = "Foo", LastName = "Bar" };
            var ruleService = new RulesService();
            var calculationService = new BenefitCostCalculatorService(ruleService);
            calculationService.CalculateBenefitCosts(employee);
            
            employee.BenefitCosts.ShouldBeEquivalentTo(1000m);
            employee.AnnualSalary.ShouldBeEquivalentTo(52000m);
            employee.PeriodBenefitCosts.ShouldBeEquivalentTo(1000m / 26);
            employee.PeriodPay.ShouldBeEquivalentTo(52000m / 26);
        }

        [Fact]
        public void CheckDiscountedEmployee()
        {
            var employee = new Employee() { FirstName = "AFoo", LastName = "Bar" };
            var ruleService = new RulesService();
            var calculationService = new BenefitCostCalculatorService(ruleService);
            calculationService.CalculateBenefitCosts(employee);

            employee.BenefitCosts.ShouldBeEquivalentTo(900m, because: "a name discount");
            employee.AnnualSalary.ShouldBeEquivalentTo(52000m, because: "default salary");
            employee.PeriodBenefitCosts.ShouldBeEquivalentTo(900m / 26, because: "discount over 26 pay periods");
            employee.PeriodPay.ShouldBeEquivalentTo(52000m / 26);
        }

        private static List<Beneficiary> ComputeCosts(Employee employee, Dependent dependent)
        {
            var beneficiaries = new List<Beneficiary>() { employee, dependent };
            var ruleService = new RulesService();
            var calculationService = new BenefitCostCalculatorService(ruleService);
            calculationService.CalculateBenefitCosts(beneficiaries);
            return beneficiaries;
        }
        
        public void CalculationVerify()
        {
            var employee = new Employee { FirstName = "A", LastName = "B" };
            var rulesService = new Mock<IRulesService>();
            var calcService = new Mock<IBenefitCostCalculatorService>(rulesService.Object);
            calcService.Setup(cs => cs.CalculateBenefitCosts(employee));
            
            calcService.Verify(cs => cs.CalculateBenefitCosts(It.Is<Employee>(e =>
                e.BenefitCosts == 900m
            )));

            rulesService.Verify(rs => rs.MatchesAny(It.Is<Employee>(e =>
                e == employee
            )), Times.Once);
        }
    }
}

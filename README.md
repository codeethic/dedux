# README #

### What is this repository for? ###

* A code challenge

### Continuous Integration ###
https://ci.appveyor.com/project/codeethic/dedux

### How do I get set up? ###

1. `git clone https://codeethic@bitbucket.org/codeethic/dedux.git dedux`
2. The solution was created using .NET Core 2.0 with Visual Studio 2017. 
	* The project files are not compatible with older versions of Visual Studio. 
	* One can view and run the solution in Visual Studio Code and the C# extension which is much less painful to install than Visual Studio 2017.
	* To build and run outside of VS environment (once core/standard dependencies are installed) execute `dotnet run` from Paylocity.Dedux.Web directory.
		* Navigate your browser to <url>:<port>/index.html
		* Swagger: <url>:<port>/swagger
3. Configuration
	* None
4. Dependencies
	* [.NET Core 2.0](https://github.com/dotnet/core/blob/master/release-notes/download-archives/2.0.0-download.md)
	* [.NET Standard 2.0](https://blogs.msdn.microsoft.com/benjaminperkins/2017/09/20/how-to-install-net-standard-2-0/)
	* ASP.NET Core 2.0
	* EF Core 2.0
5. Database configuration
	* None: In-Memory 

### Things to know for UI dev ###

1. UI Stack
	* react
	* redux
	* npm: 3.10.10
    * node: 6.11.3
2. npm commands - from paylocity-dedux-react directory
	* `npm install`
	* `npm start`
		- Builds and starts the development server
	* `npm run build`
		- Bundles the app into static files for production

### Unit tests ###
* The tests leverage xUnit. 
* execute `dotnet test` to run tests from project root directory.

### Who do I talk to? ###

* Joshua Friend - joshua@codeethic.com
﻿using Paylocity.Dedux.Core.BenefitCostCalculators;

namespace Paylocity.Dedux.Core.Model
{
    public class Dependent :  Beneficiary
    {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public override IBenefitCostCalculator CostCalculator => new DependentBenefitCalculator(this);
    }
}

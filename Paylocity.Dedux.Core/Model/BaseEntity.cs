﻿namespace Paylocity.Dedux.Core.Model
{
    public abstract class BaseEntity : IEntity
    {
        public int Id { get; set; }
    }
}

﻿namespace Paylocity.Dedux.Core.Model
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}

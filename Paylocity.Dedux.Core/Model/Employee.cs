﻿using Paylocity.Dedux.Core.BenefitCostCalculators;
using System.Collections.Generic;

namespace Paylocity.Dedux.Core.Model
{
    public class Employee : Beneficiary
    {
        public virtual List<Dependent> Dependents { get; set; }
        public override IBenefitCostCalculator CostCalculator => new EmployeeBenefitCalculator(this);
        public decimal AnnualSalary { get; internal set; }
        public decimal PeriodPay { get; internal set; }
    }
}

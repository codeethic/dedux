﻿using Paylocity.Dedux.Core.BenefitCostCalculators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paylocity.Dedux.Core.Model
{
    public abstract class Beneficiary : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal BenefitCosts { get; set; }
        public decimal PeriodBenefitCosts { get; internal set; }
        public abstract IBenefitCostCalculator CostCalculator { get; }
    }
}

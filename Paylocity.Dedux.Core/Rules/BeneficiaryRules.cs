﻿using System;
using System.Collections.Generic;
using Paylocity.Dedux.Core.Model;

namespace Paylocity.Dedux.Core.Rules
{
    public interface IRulesService
    {
        bool MatchesAny(Beneficiary beneficiary);
    }

    public interface IBeneficiaryRule
    {        
        Func<Beneficiary, bool> Predicate { get; set; }
    }

    public class BeneficiaryRule : IBeneficiaryRule
    {
        public Func<Beneficiary, bool> Predicate { get; set; }
        
        public BeneficiaryRule(Func<Beneficiary, bool> predicate)
        {
            Predicate = predicate;
        }
    }
}
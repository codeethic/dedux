﻿using Paylocity.Dedux.Core.Rules;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paylocity.Dedux.Core.BenefitCostCalculators
{
    public interface IBenefitCostCalculator
    {
        void Calculate(IRulesService rulesService);
    }
}

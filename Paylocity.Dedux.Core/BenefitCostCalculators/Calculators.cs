﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Core.Rules;

namespace Paylocity.Dedux.Core.BenefitCostCalculators
{
    public abstract class BaseBenefitCalculator : IBenefitCostCalculator
    {
        decimal _defaultDiscountPercent = 10m;
        protected int DefaultPayChecksPerYear = 26;

        public BaseBenefitCalculator(Beneficiary beneficiary)
        {
            Beneficiary = beneficiary;
            DiscountPercent = _defaultDiscountPercent;
        }

        public decimal DiscountPercent
        {
            get;
            set;
        }

        protected Beneficiary Beneficiary { get; set; }
        public decimal InitialCost { get; set; } 
        public abstract void Calculate(IRulesService rulesService);
        protected void CalculateDiscount(IRulesService rulesService)
        {
            if (rulesService.MatchesAny(Beneficiary))
            {
                var discount = Beneficiary.BenefitCosts * DiscountPercent / 100;
                Beneficiary.BenefitCosts = Beneficiary.BenefitCosts - discount; 
            }
        }
    }

    public class EmployeeBenefitCalculator : BaseBenefitCalculator
    {
        decimal _defaultCost = 1000;
        decimal _defaultPeriodPay = 2000;
                
        public EmployeeBenefitCalculator(Beneficiary beneficiary) : base(beneficiary)
        {
            InitialCost = _defaultCost;
            PeriodPay = _defaultPeriodPay;
        }

        public decimal PeriodPay { get; set; }

        public override void Calculate(IRulesService rulesService)
        {
            var employee = Beneficiary as Employee;
            employee.BenefitCosts = InitialCost;
            employee.AnnualSalary = PeriodPay * DefaultPayChecksPerYear;
            employee.PeriodPay = PeriodPay;
            CalculateDiscount(rulesService);
            employee.PeriodBenefitCosts = employee.BenefitCosts / DefaultPayChecksPerYear;
        }
    }

    public class DependentBenefitCalculator : BaseBenefitCalculator
    {
        decimal _defaultCost = 500;

        public DependentBenefitCalculator(Beneficiary beneficiary) : base(beneficiary)
        {
            InitialCost = _defaultCost;
        }

        public override void Calculate(IRulesService rulesService)
        {
            Beneficiary.BenefitCosts = InitialCost; 
            CalculateDiscount(rulesService);
            Beneficiary.PeriodBenefitCosts = Beneficiary.BenefitCosts / DefaultPayChecksPerYear;
        }
    }
}

﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Core.Rules;
using System.Collections.Generic;
using System.Linq;

namespace Paylocity.Dedux.Services
{
    public interface IBenefitCostCalculatorService
    {
        void CalculateBenefitCosts(Beneficiary beneficiary);
        void CalculateBenefitCosts(IEnumerable<Beneficiary> beneficiaries);
    }

    public class BenefitCostCalculatorService : IBenefitCostCalculatorService
    {
        IRulesService _rulesService;
        
        public BenefitCostCalculatorService(IRulesService rulesService)
        {
            _rulesService = rulesService;
        }
        
        public void CalculateBenefitCosts(IEnumerable<Beneficiary> beneficiaries)
        {
            foreach(var beneficiary in beneficiaries)
            {
                beneficiary.CostCalculator.Calculate(_rulesService);
            }
        }

        public void CalculateBenefitCosts(Beneficiary beneficiary)
        {
            beneficiary.CostCalculator.Calculate(_rulesService);
        }
    }
}

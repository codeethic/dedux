﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Data;
using Paylocity.Dedux.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Services
{
    public interface IQueryService<TEntity> 
    {
        Task AddAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task<List<TEntity>> FindAllByAsync(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(int id);
        Task UpdateAsync(TEntity entity);        
    }

    public class QueryService<TEntity> : BaseService, IQueryService<TEntity> where TEntity : class, IEntity
    {
        public QueryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            
        }
        
        public async Task AddAsync(TEntity entity)
        {
            await ExecuteCommitTransaction(() => Repository.Add(entity));
        }

        public async Task DeleteAsync(TEntity entity)
        {            
            await ExecuteCommitTransaction(() =>
            {                
                Repository.Delete(entity);
            });
        }

        public async Task<List<TEntity>> FindAllByAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Repository.FindAllByAsync(predicate);
        }

        public async Task<List<TEntity>> GetAllAsync() 
        {
            return await Repository.GetAllAsync();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await Repository.GetByIdAsync(id);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            await ExecuteCommitTransaction(() => Repository.Update(entity));            
        }
        
        public IRepository<TEntity> Repository => UnitOfWork.GetRepository<TEntity>();
    }
}

﻿using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Core.Rules;
using System;
using System.Collections.Generic;

namespace Paylocity.Dedux.Services 
{
    public class RulesService : IRulesService
    {
        List<IBeneficiaryRule> _rules = new List<IBeneficiaryRule>();

        public RulesService()
        {
            _rules.Add(DefaultRule);
        }

        private IBeneficiaryRule DefaultRule
        {
            get
            {
                Func<Beneficiary, bool> discountFunc = (b) => b.FirstName.StartsWith("a", StringComparison.InvariantCultureIgnoreCase) ||
                                                              b.LastName.StartsWith("a", StringComparison.InvariantCultureIgnoreCase);

                return new BeneficiaryRule(discountFunc);
            }            
        }

        public void AddRule(IBeneficiaryRule rule)
        {
            _rules.Add(rule);
        }
        
        public bool MatchesAny(Beneficiary beneficiary)
        {            
            foreach (var rule in _rules)
            {
                if (rule.Predicate(beneficiary))
                {
                    return true;
                };
            }

            return false;
        }
    }
}

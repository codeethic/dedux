﻿using Paylocity.Dedux.Data;
using System;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Services
{
    public abstract class BaseService
    {
        protected IUnitOfWork UnitOfWork { get; }

        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public async Task ExecuteCommitTransaction(Action action)
        {
            using (var transaction = UnitOfWork.BeginTransaction())
            {
                try
                {
                    action();
                    await UnitOfWork.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}

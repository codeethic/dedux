﻿namespace Paylocity.Dedux.Web.DataTransferObjects
{
    public class EmployeeDto : BaseBenficiaryDto
    {
        public decimal AnnualSalary { get; set; }
        public decimal PeriodPay { get; set; }
    }
}

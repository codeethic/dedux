﻿using System.ComponentModel.DataAnnotations;

namespace Paylocity.Dedux.Web.DataTransferObjects
{
    public class DependentDto : BaseBenficiaryDto
    {
        [Required]
        public int EmployeeId { get; set; }
        public EmployeeDto EmployeeDto { get; set; }
    }
}

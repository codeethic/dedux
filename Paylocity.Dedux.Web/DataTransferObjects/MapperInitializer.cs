﻿using AutoMapper;
using Paylocity.Dedux.Core.Model;

namespace Paylocity.Dedux.Web.DataTransferObjects
{
    public class MapperInitializer
    {
        public void Initialize()
        {
            Mapper.Initialize(config => {
                config.CreateMap<Employee, EmployeeDto>().ReverseMap();
                config.CreateMap<Dependent, DependentDto>().ReverseMap();
            });
        }
    }
}

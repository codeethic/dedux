﻿using System.ComponentModel.DataAnnotations;

namespace Paylocity.Dedux.Web.DataTransferObjects
{
    public class BaseDto
    {
        public int Id { get; set; }
    }

    public class BaseBenficiaryDto : BaseDto
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public decimal BenefitCosts { get; set; }
        public decimal PeriodBenefitCosts { get; set; }
    }
}

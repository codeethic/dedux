﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Paylocity.Dedux.Data;
using Paylocity.Dedux.Services;
using Swashbuckle.AspNetCore.Swagger;
using Paylocity.Dedux.Web.DataTransferObjects;
using Paylocity.Dedux.Core.Rules;
using System.IO;

namespace Dedux
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddCors();
            services.AddDbContext<AppDbContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IQueryService<>), typeof(QueryService<>));
            services.AddScoped(typeof(IRulesService), typeof(RulesService));
            services.AddScoped(typeof(IBenefitCostCalculatorService), typeof(BenefitCostCalculatorService));

            services.AddTransient<DbInitializer>();
            services.AddTransient<MapperInitializer>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Dedux", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                              IHostingEnvironment env, 
                              ILoggerFactory loggerFactory, 
                              DbInitializer dbInitializer,
                              MapperInitializer mapperInitializer)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(builder => builder.AllowAnyHeader()
                                          .AllowAnyOrigin()
                                          .AllowAnyMethod());

            
            UseSwagger(app);
            app.UseStaticFiles();
            app.UseMvc();
            dbInitializer.Seed();
            mapperInitializer.Initialize();
        }

        private void UseSwagger(IApplicationBuilder app)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dedux API V1");
            });
        }
    }
}

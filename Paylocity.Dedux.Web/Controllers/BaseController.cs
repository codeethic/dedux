﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Services;
using Paylocity.Dedux.Web.DataTransferObjects;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Web.Controllers
{
    public abstract class BaseController<TEntity, TDto> : Controller where TEntity : class, IEntity 
                                                            where TDto : BaseDto
    {
        protected IQueryService<TEntity> QueryService { get; private set; }
        protected IBenefitCostCalculatorService CalculatorService { get; private set; }
        
        public BaseController(IQueryService<TEntity> queryService, IBenefitCostCalculatorService calculatorService)
        {
            QueryService = queryService;
            CalculatorService = calculatorService;
        }

        [HttpGet]
        public async Task<IEnumerable<TDto>> Get()
        {
            var entityList = await QueryService.GetAllAsync();
            return Mapper.Map<IEnumerable<TEntity>, IEnumerable<TDto>>(entityList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await QueryService.GetByIdAsync(id);
            if(entity == null)
            {
                return NotFound();
            }
            var dto = Mapper.Map<TEntity, TDto>(entity);
            return Ok(dto);
        }

        [HttpPost]
        public virtual async Task<IActionResult> Post([FromBody] TDto dto)
        {
            if (dto == null)
            {
                return BadRequest();
            }

            var entity = Mapper.Map<TDto, TEntity>(dto);
            await QueryService.AddAsync(entity);
            var returnDto = Mapper.Map<TEntity, TDto>(entity);
            return new ObjectResult(returnDto) { StatusCode = (int)HttpStatusCode.Created }; 
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] TDto dto)
        {
            if (dto == null)
            {
                return BadRequest();
            }

            var entity = await QueryService.GetByIdAsync(dto.Id);
            if(entity == null)
            {
                return NotFound();
            }
            
            Mapper.Map(dto, entity);
            await QueryService.UpdateAsync(entity);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await QueryService.GetByIdAsync(id);
            if(entity == null)
            {
                return NotFound();
            }
            await QueryService.DeleteAsync(entity);
            return NoContent();
        }
    }
}

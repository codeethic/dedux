﻿using Microsoft.AspNetCore.Mvc;
using Paylocity.Dedux.Services;
using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Web.DataTransferObjects;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;

namespace Paylocity.Dedux.Web.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : BaseController<Employee, EmployeeDto>
    {
        public EmployeesController(IQueryService<Employee> queryService,
                                   IBenefitCostCalculatorService calculatorService) : base(queryService, calculatorService)
        {
            
        }
    }
}

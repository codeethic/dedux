﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Paylocity.Dedux.Core.Model;
using Paylocity.Dedux.Services;
using Paylocity.Dedux.Web.DataTransferObjects;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Paylocity.Dedux.Web.Controllers
{
    [Route("api/[controller]")]
    public class DependentsController : BaseController<Dependent, DependentDto>
    {
        IQueryService<Employee> _employeeService;
        
        public DependentsController(IQueryService<Dependent> dependentService, 
                                    IQueryService<Employee> employeeService,
                                    IBenefitCostCalculatorService calculatorService) : base(dependentService, calculatorService)
        {
            _employeeService = employeeService;
        }

        [HttpPost]
        public override async Task<IActionResult> Post([FromBody] DependentDto dto)
        {
            if (dto == null)
            {
                return BadRequest();
            }

            var entity = Mapper.Map<DependentDto, Dependent>(dto);
            await QueryService.AddAsync(entity);
            CalculatorService.CalculateBenefitCosts(entity);
            var returnDto = Mapper.Map<Dependent, DependentDto>(entity);
            return new ObjectResult(returnDto) { StatusCode = (int)HttpStatusCode.Created };
        }

        [HttpGet("policyHolder/{employeeId}")]
        public async Task<IActionResult> GetPolicyHolderDependents(int employeeId)
        {
            var dependents = await QueryService.FindAllByAsync(d => d.EmployeeId == employeeId);            
            var employee = await _employeeService.GetByIdAsync(employeeId);

            var beneficieries = new List<Beneficiary>();
            beneficieries.AddRange(dependents);
            beneficieries.Add(employee);
            CalculatorService.CalculateBenefitCosts(beneficieries);

            var employeeDto = Mapper.Map<Employee, EmployeeDto>(employee);
            var dependentsDto = Mapper.Map<IEnumerable<Dependent>, IEnumerable<DependentDto>>(dependents);
            var responseDto = new { Dependents = dependentsDto, Employee = employeeDto };

            return Ok(responseDto);
        }
    }
}
